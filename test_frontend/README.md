# test_frontend

## Installation

```bash
$ yarn
```

## Running the app

```bash
# development
$ yarn start

# production build
$ yarn build
```

## Test

```bash
# unit tests
$ yarn test

```