import React from 'react';
import TableIcon from './TableIcon.component'
import { format, isToday, isYesterday } from 'date-fns'
import './Table.css'

export default function Table({ stories, onIconClick, onRowClick }) {

  const renderTime = story => {
    let date = new Date(story.createdAt)
    return isToday(date) ? format(date, 'HH:mm aaa') : isYesterday(date) ? 'Yesterday' : format(date, 'LLL d')
  }

  const handleRowClick = story => e => {
    e.stopPropagation()
    onRowClick(e, story)
  }

  const handleIconClick = story => e => {
    e.stopPropagation()
    onIconClick(e, story)
  }

  return (
    <table className="table">
      <tbody>
        {
          stories.map((story, key) => 
            <tr className="row" key={ key } onClick={ handleRowClick(story) }>
              <td>
                <span className="title">{ story.title }</span>
                <span className="author"> - { story.author } - </span>
              </td>
              <td>
                <span className="time">{ renderTime(story) }</span> 
                <span className="icon" onClick={ handleIconClick(story) }>
                  <TableIcon/>
                </span>
              </td>
            </tr>
          )
        }
      </tbody>
    </table>
  )
}

Table.defaultProps = {
  stories: [],
  onIconClick: () => {},
  onRowClick: () => {},
}