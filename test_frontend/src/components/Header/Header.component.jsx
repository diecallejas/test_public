import React from 'react'
import './Header.css'

export default function Header({ title, subtitle }) {
  return (
    <header className="header">
      <h1 className="title">{ title }</h1>
      <h3 className="subtitle">{ subtitle }</h3>
    </header>
  )
}

Header.defaultProps = {
  title: "HN Feed",
  subtitle: "We  <3 hacker news!",
}
