import React, { Component } from 'react';

import Table from '../components/Table/Table.component'

export default class Home extends Component {

  constructor(props) {
    super(props);
    this.state = {
      stories: []
    };
  }

  componentDidMount() {
    this.getStories()
  }

  deleteStory(story) {
    fetch('http://localhost:8000/stories/' + story._id, { method: 'delete' }).then(res => {
      this.getStories()
    })
  }

  getStories() {
    fetch('http://localhost:8000/stories').then(async res => {
      let stories = await res.json()
      this.setStories(stories)
    })
  }

  handleTableIcoClick = (event, story) => {
    this.deleteStory(story)
  }

  handleTableRowClick = (event, story) => {
    this.openStory(story)
  }

  setStories(stories = []) {
    this.setState({
      stories,
    })
  }

  openStory(story) {
    window.open(story.url, '_blank')
  }

  render() {
    const { stories } = this.state
    return (
      <Table 
        stories={ stories } 
        onIconClick={ this.handleTableIcoClick }
        onRowClick={ this.handleTableRowClick }
      />
    )
  }
}