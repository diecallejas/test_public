// import logo from './logo.svg';
// import './App.css';

import Header from './components/Header/Header.component'
import Home from './views/Home'

function App() {
  return (
    <div className="app">
      <Header/>
      <Home/>
    </div>
  );
}

export default App;
