# test_public

## Running the container

```bash

# build container
$ docker compose build

# run container
$ docker compose up

# down container
$ docker compose down
```