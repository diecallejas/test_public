import { Controller, Delete, Get, Param, Logger } from '@nestjs/common';
import { Interval } from '@nestjs/schedule';
import { StoriesService } from './stories.service';

@Controller('stories')
export class StoriesController {
  private readonly logger = new Logger(StoriesController.name)

  constructor(private readonly storiesService: StoriesService) {}

  @Get()
  findAll() {
    return this.storiesService.findAll()
  }

  @Delete(':id')
  read(@Param('id') id: number) {
    return this.storiesService.read(id)
  }

  @Interval(60*60*1000)
  sync() {
    this.logger.log('Pulling data from hn.algolia.com, every 1 hour')
    this.storiesService.sync()
  }

  onModuleInit() {
    this.sync()
  }
}
