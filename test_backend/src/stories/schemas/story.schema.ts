import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type StoryDocument = Story & Document;

@Schema()
export class Story {

  @Prop()
  _id: number;

  @Prop()
  author: string;

  @Prop()
  createdAt: Date;
  
  @Prop({ default: false })
  read: boolean;
  
  @Prop()
  title: string;

  @Prop()
  url: string;
}

export const StorySchema = SchemaFactory.createForClass(Story);