import { HttpService } from '@nestjs/common';
import { plainToClass } from 'class-transformer';
import { HttpStoryDto } from './dto/http-story.dto';

export class StoriesHttp {
  private storyHttp = new HttpService()
  private httpUrl = 'https://hn.algolia.com/api/v1/search_by_date?query=nodejs'

  async get() : Promise<HttpStoryDto[]> {
    return this.storyHttp.get(this.httpUrl).toPromise().then(res => {
      return res.data.hits.map((hit: object) => {
        return plainToClass(HttpStoryDto, hit)
      })
    })
  }
}