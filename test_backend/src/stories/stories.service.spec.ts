import { getModelToken } from '@nestjs/mongoose';
import { Test, TestingModule } from '@nestjs/testing';
import { Model } from 'mongoose';
import { Story, StoryDocument } from './schemas/story.schema'; 
import { StoriesService } from './stories.service';

describe('StoriesService', () => {
  let service: StoriesService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        { 
          provide: getModelToken(Story.name),
          useValue: Model,
        },
        StoriesService,
      ],
    }).compile();

    service = module.get<StoriesService>(StoriesService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('should be read a story', async () => {
    const storyId = 123456;
    const spy = jest
      .spyOn(service, 'read') 
      .mockResolvedValue({
        ok: 1,
        n: 1,
        nModified: 1,
      });
    let response = await service.read(storyId);
    expect(response.n).toEqual(1);
    expect(response.nModified).toEqual(1);
    expect(response.ok).toEqual(1);
  });

  it('should return a story collection', async () => {
    const story = new Story();
    const spy = jest
      .spyOn(service, 'findAll') 
      .mockResolvedValue([story] as StoryDocument[]);
    await service.findAll();
    expect(spy).toBeCalled();
  });
});
