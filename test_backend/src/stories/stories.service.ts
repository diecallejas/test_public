import { Model } from 'mongoose';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { StoriesHttp } from './stories.http'
import { Story, StoryDocument } from './schemas/story.schema';

@Injectable()
export class StoriesService {
  private readonly storiesHttp = new StoriesHttp();

  constructor(@InjectModel(Story.name) private storyModel: Model<StoryDocument>) {}

  findAll() {
    return this.storyModel.find({ read: false }).sort({ createdAt: "descending" }).exec()
  }

  read(storyId: number) {
    return this.storyModel.updateOne({ _id: storyId }, { read: true }).exec()
  }

  async sync() {

    let stories = await this.storiesHttp.get()

    stories.forEach(async story => {
      let exists = await this.storyModel.exists({ _id: story._id })
      if (!exists) {
        await new this.storyModel(story).save()
      }
    })
  }
}
