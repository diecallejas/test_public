
import { plainToClass } from 'class-transformer';
import { HttpStoryDto } from './http-story.dto';

describe('StorySchema', () => {
  it('should be defined', () => {
    expect(new HttpStoryDto({})).toBeDefined()
  });
  
  it('should be defined dto attributes with normal data', () => {
    let story = plainToClass(HttpStoryDto, {
      objectID: "343434",
      created_at: "2020-10-10",
      title: "title",
      url: "http://localhost:8080/foo",
      author: "bar",
    })
    
    expect(story._id).toBeDefined()
    expect(story.author).toBeDefined()
    expect(story.createdAt).toBeDefined()
    expect(story.title).toBeDefined()
    expect(story.url).toBeDefined()
    expect(story.author).toEqual("bar")
    expect(story.createdAt).toEqual(new Date("2020-10-10"))
    expect(story.title).toEqual("title")
    expect(story.url).toEqual("http://localhost:8080/foo")
  });
  
  it('should be defined dto attributes with null data', () => {
    let story = plainToClass(HttpStoryDto, {
      objectID: "343434",
      created_at: "2020-10-10",
      title: null,
      url: null,
      author: null,
      story_title: "title",
      story_url: "http://localhost:8080/foo",
      story_author: "bar",
    })
    
    expect(story._id).toBeDefined()
    expect(story.author).toBeDefined()
    expect(story.createdAt).toBeDefined()
    expect(story.title).toBeDefined()
    expect(story.url).toBeDefined()
    expect(story.author).toEqual("bar")
    expect(story.createdAt).toEqual(new Date("2020-10-10"))
    expect(story.title).toEqual("title")
    expect(story.url).toEqual("http://localhost:8080/foo")
  });
  
  it('should be defined url from story_id', () => {
    let story = plainToClass(HttpStoryDto, {
      story_id: "343434",
    })
    expect(story.url).toEqual("https://news.ycombinator.com/item?id=343434")
  });
  
  it('should be defined url from objectID', () => {
    let story = plainToClass(HttpStoryDto, {
      objectID: "343434",
    })
    expect(story.url).toEqual("https://news.ycombinator.com/item?id=343434")
  });
});
