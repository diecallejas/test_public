import { Exclude, Expose, Transform } from "class-transformer";

@Exclude()
export class HttpStoryDto {

  @Expose()
  @Transform(({ obj }) => parseInt(obj.objectID) )
  _id: number;

  @Expose()
  @Transform(({ obj }) => obj.author || obj.story_author )
  author: string;

  @Expose()
  @Transform(({ obj }) => new Date(obj.created_at) )
  createdAt: Date;
  
  @Expose()
  @Transform(({ obj }) => obj.title || obj.story_title )
  title: string;

  @Expose()
  @Transform(({ obj }) => obj.url || obj.story_url || 'https://news.ycombinator.com/item?id=' + (obj.story_id ||obj.objectID) )
  url: string;

  constructor(partial: Partial<HttpStoryDto>) {
    Object.assign(this, partial);
  }
}