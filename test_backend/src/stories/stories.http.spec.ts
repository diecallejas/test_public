import { StoriesHttp } from './stories.http';

describe('StoriesService', () => {
  let storiesHttp: StoriesHttp;

  beforeEach(async () => {
    storiesHttp = new StoriesHttp()
  });

  it('should be defined', () => {
    expect(storiesHttp).toBeDefined();
  });

  it('should be read a story', async () => {
    const spy = jest
      .spyOn(storiesHttp, 'get') 
      .mockResolvedValue([
        {
          createdAt: new Date(),
          title: "null",
          url: "null",
          author: "null",
          _id: 1,
        }
      ]);
    let response = await storiesHttp.get();
    expect(response.length).toEqual(1);
  });
});
